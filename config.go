package turtle

// Config represents a turtle configuration
type Config struct {
	Files          []Fileset `json:"files"`
	Keywords       []string  `json:"keywords"`
	InspectContent bool      `json:"inspectContent"`
}

// Fileset represents a directory to be examined by turtle and configuration about files to in- or exclude
type Fileset struct {
	Path      string   `json:"path"`
	Include   []string `json:"include"`
	Exclude   []string `json:"exclude"`
	Recursive bool     `json:"recursive"`
}
