package turtle

import (
	"bufio"
	"bytes"
	"compress/bzip2"
	"compress/gzip"
	"errors"
	"io"
	"os"
)

// ErrBinaryFile indicates that the file is probably binary and has not been opened
var ErrBinaryFile = errors.New("File contents look binary")

var gzipMagic = []byte{0x1f, 0x8b, 0x08}
var bzip2Magic = []byte("BZh")

// Open opens a file if it is plain or compressed text
func Open(f string) (io.ReadCloser, error) {
	file, err := os.Open(f)
	if err != nil {
		return nil, err
	}

	unpacked, err := unpack(file)
	if err != nil {
		return nil, err
	}

	bin, err := looksBinary(unpacked)
	if err != nil {
		if err == io.EOF {
			// heuristic did not work because the file is too small

		} else {
			return nil, err
		}
	}
	if bin {
		return nil, ErrBinaryFile
	}

	return unpacked, nil
}

// unpack attempts to unpack gzip- or bzip-compressed content and returns an unpacked io.ReadCloser
func unpack(r io.ReadCloser) (io.ReadCloser, error) {
	buf := bufio.NewReader(r)

	// peek first three bytes
	magic, err := buf.Peek(3)
	if err != nil {
		if err == io.EOF {
			return newWrappedReader(buf, r), nil
		}
		return nil, err
	}

	if bytes.Equal(magic, gzipMagic) {
		//gzip
		gzReader, err := gzip.NewReader(buf)
		if err != nil {
			return nil, err
		}

		//go deeper...
		return unpack(gzReader)
	} else if bytes.Equal(magic, bzip2Magic) {
		//bzip2
		bzReader := bzip2.NewReader(buf)

		//go deeper...
		return unpack(newWrappedReader(bzReader, r))
	}
	return newWrappedReader(buf, r), nil
}

// looksBinary attempts to determine if a file looks binary
func looksBinary(r io.Reader) (bool, error) {
	// TODO implement some magic
	return false, nil
}
