package turtle

import (
	"errors"
	"os"
	"path/filepath"
)

// Walker walks a source of strings
// It's intended to be used in a loop: check if HasMore() returns no errors and is true, then call Walk()
type Walker interface {
	// Walk advances the Walker over the next element and returns it
	Walk() string
	// HasMore checks if more elements are available
	HasMore() (bool, error)
}

// ErrNoMoreEntries indicates that no more entries are present from the source.
// The error is only thrown if by Walk() if HasMore() has previously returned false
var ErrNoMoreEntries = errors.New("No more entries")

type filesetWalker struct {
	files        []Fileset
	index        int
	current      []string
	currentIndex int
}

// NewFilesetWalker creates a new Walker using the specified slice of Filesets
func NewFilesetWalker(f []Fileset) Walker {
	return &filesetWalker{
		files: f,
		index: -1,
	}
}

func (w *filesetWalker) Walk() string {
	if ok, err := w.HasMore(); err != nil || !ok {
		panic(ErrNoMoreEntries)
	}

	s := w.current[w.currentIndex]
	w.currentIndex++
	if w.currentIndex >= len(w.current) {
		w.current = nil
	}

	return s
}

func (w *filesetWalker) openNextFileset() error {
	w.index++
	if w.index >= len(w.files) {
		return ErrNoMoreEntries
	}

	fs := w.files[w.index]

	hasIncludes := fs.Include != nil && len(fs.Include) > 0
	hasExcludes := fs.Exclude != nil && len(fs.Exclude) > 0

	var newFiles []string

	filepath.Walk(fs.Path, func(path string, f os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if f.IsDir() {
			if path != fs.Path && !fs.Recursive {
				return filepath.SkipDir
			}
			return nil
		}
		if hasIncludes {
			// has includes, only accept those
			for _, s := range fs.Include {
				if s == path[len(fs.Path)+1:] {
					newFiles = append(newFiles, path)
				}
			}
		} else if hasExcludes {
			// has excludes, exclude those
			var exclude bool
			for _, s := range fs.Exclude {
				if s == path[len(fs.Path)+1:] {
					exclude = true
					break
				}
			}
			if !exclude {
				newFiles = append(newFiles, path)
			}
		} else {
			// just add it
			newFiles = append(newFiles, path)
		}

		return nil
	})

	if len(newFiles) == 0 {
		return w.openNextFileset()
	}

	w.current = newFiles
	w.currentIndex = 0
	return nil
}

func (w *filesetWalker) HasMore() (bool, error) {
	if w.current == nil {
		err := w.openNextFileset()
		if err != nil {
			if err == ErrNoMoreEntries {
				return false, nil
			}
			return false, err
		}
	}

	return (w.currentIndex < len(w.current) || w.index < len(w.files)), nil
}
