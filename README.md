# turtle

`turtle` is a program that searches file contents and names for given keywords. It supports plain text files, gzipped
files and bzip2-compressed files. Turtle is written in Go.

## Downloading `turtle`

To download and build turtle, you need a go>=1.5 installation. If you set up everything correctly

    go get -u bitbucket.org/mrd0ll4r/turtle/cmd/turtle

will get and build the program. This places the `turtle` executable in you `$GOPATH/bin` folder, so (assuming you set up
everything correctly) you can start it by simply executing `turtle`.

## Usage

Turtle is intended to be used on a regular basis, e.g. in a cron job. It therefore stores its configuration in a file.  
The command line syntax of `turtle` is as follows:

    -c string
        config file (default "~/.turtle.json")
    -o string
        file to log to, leave empty for Stdout

## Configuration

Turtle uses a single JSON configuration file. An example is included in `exampleConfig.json`. Following is a (non valid
JSON!) explanation:

    {
      "files": [ //list of directories to scan
        {
          "path": "/opt", // directory (non recursive)
          "include": null, // list of files to explicitly include (all others will be ignored)
          "exclude": null // list of files to explicitly ignore (all other files will be included)
        },
        {
          "path": "/media",
          "include": null,
          "exclude": ["usb/personal"], // plain text, no regular expressions, relative to path, i.e. /media/usb/personal/* is excluded
          "recursive": true
        }
        // more directories...
      ],
      "keywords": [ // list of keywords to search for
        "Invoice", // plain text, no regular expressions
        "invoice",
        "Order",
        "order",
        "Bill",
        "bill"
        // more keywords
      ],
      "inspectContent": true // whether to inspect file contents or just file names
    }

## Example output

This is an example of what Turtle logs:

    2015/12/08 16:01:19 Inspecting the turtle...
    2015/12/08 16:01:19 Content Match: D:\turtletest\big\8EQ36R.pdf (order)
    2015/12/08 16:01:19 Content Match: D:\turtletest\big\AUTHORS (Bill)
    2015/12/08 16:01:19 Content Match: D:\turtletest\big\BS8WWW.pdf (order)
    2015/12/08 16:01:19 Content Match: D:\turtletest\big\CONTRIBUTORS (Bill)
    2015/12/08 16:01:21 Content Match: D:\turtletest\big\GWInstall.jar (order)
    2015/12/08 16:01:21 Content Match: D:\turtletest\big\GWSetup.jar (Order)
    2015/12/08 16:01:21 Content Match: D:\turtletest\big\GWShared.jar (order)
    2015/12/08 16:01:21 Content Match: D:\turtletest\big\PATENTS (order)
    2015/12/08 16:01:21 Content Match: D:\turtletest\big\WinUtils.jar (Order)
    2015/12/08 16:01:21 Content Match: D:\turtletest\big\XUtils.jar (Order)
    2015/12/08 16:01:21 Content Match: D:\turtletest\big\all_test.go (order)
    2015/12/08 16:01:22 Content Match: D:\turtletest\big\deepequal.go (order)
    2015/12/08 16:01:22 Content Match: D:\turtletest\big\type.go (order)
    2015/12/08 16:01:22 Content Match: D:\turtletest\big\value.go (order)
    2015/12/08 16:01:22 Took 2.8562856s, processed 83 files, 19901052 bytes
    2015/12/08 16:01:22 Inspection finished, some parts stickin' out!

This is an output from a clean set of directories:

    2015/12/08 16:03:08 Inspecting the turtle...
    2015/12/08 16:03:08 Took 12.0012ms, processed 5 files, 30744 bytes
    2015/12/08 16:03:08 Inspection finished, looking good.

## How does it work?

File name matching is straightforward: look if the full path (not necessarily absolute) contains any keyword.  
Content matching is done on streams: The file content is read completely through a series of readers that implement a 
sliding window algorithm to search for one keyword each. This enables us to never hold the file in memory. Time 
complexity is linear, memory consumption constant.

## Why the name?

In case it's not obvious from the logs: We are inspecting a turtle (the system) to see whether parts (files) stick out
of the shell (the encrypted part of the file system). So, if the turtle sits nicely inside its shell, not peeking out,
it's safe. Same goes for the system: If all files with sensitive content are in the encrypted parts of the file system,
everything is fine.

## License

Copyright (c) 2015 mrd0ll4r

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
documentation files (the "Software"), to deal in the Software without restriction, including without limitation the 
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the 
Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE 
WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR 
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
