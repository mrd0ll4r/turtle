package turtle

import (
	"os"
	"testing"
)

var testConfig = Config{
	Files: []Fileset{
		Fileset{
			Path: "testdata",
		},
		Fileset{
			Path:    "testdata",
			Include: []string{"a"},
		},
		Fileset{
			Path:    "testdata",
			Exclude: []string{"a"},
		},
		Fileset{
			Path:    "testdata" + string(os.PathSeparator) + "empty",
			Exclude: []string{".gitignore"},
		},
		Fileset{
			Path:      "testdata" + string(os.PathSeparator) + "recursive",
			Exclude:   []string{"dir" + string(os.PathSeparator) + "c"},
			Recursive: true,
		},
	},
}

var testWalker = NewFilesetWalker(testConfig.Files)

func TestConfigWalker(t *testing.T) {
	if ok, err := testWalker.HasMore(); err != nil || !ok {
		if err != nil {
			t.Fatal("Walker has no more entries at the start", err)
		} else {
			t.Fatal("Walker has no more entries at the start")
		}
	}

	var got []string
	for {
		ok, err := testWalker.HasMore()
		if err != nil {
			t.Fatalf("Error walking:", err)
		}
		if !ok {
			break
		}
		path := testWalker.Walk()
		got = append(got, path)
	}

	aCount := 0
	bCount := 0
	cCount := 0
	recurACount := 0
	recurBCount := 0
	for _, s := range got {
		switch s {
		case "testdata" + string(os.PathSeparator) + "a":
			aCount++
		case "testdata" + string(os.PathSeparator) + "b":
			bCount++
		case "testdata" + string(os.PathSeparator) + "c":
			cCount++
		case "testdata" + string(os.PathSeparator) + "recursive" + string(os.PathSeparator) + "a":
			recurACount++
		case "testdata" + string(os.PathSeparator) + "recursive" + string(os.PathSeparator) + "dir" + string(os.PathSeparator) + "b":
			recurBCount++
		default:
			t.Fatalf("Got unexpected entry: %s", s)
		}
	}

	if aCount != 2 {
		t.Fatal("testdata/a did not appear 2 times, but", aCount)
	}

	if bCount != 2 {
		t.Fatal("testdata/b did not appear 2 times, but", bCount)
	}

	if cCount != 2 {
		t.Fatal("testdata/c did not appear 2 times, but", cCount)
	}

	if recurACount != 1 {
		t.Fatal("testdata/recursive/a did not appear one time, but", recurACount)
	}

	if recurBCount != 1 {
		t.Fatal("testdata/recursive/dir/b did not appear one time, but", recurBCount)
	}
}
