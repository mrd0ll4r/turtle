package main

import (
	"bitbucket.org/mrd0ll4r/turtle"
	"encoding/json"
	"flag"
	"io"
	"io/ioutil"
	"log"
	"os"
	"strings"
	"sync/atomic"
	"time"
)

var (
	config    string
	logFile   string
	fileCount *uint64
	byteCount *uint64
)

func init() {
	flag.StringVar(&config, "c", "~/.turtle.json", "config file")
	flag.StringVar(&logFile, "o", "", "file to log to, leave empty for Stdout")

	var f uint64
	fileCount = &f
	var b uint64
	byteCount = &b
}

func main() {
	flag.Parse()

	if logFile != "" {
		out, err := os.Create(logFile)
		if err != nil {
			panic(err)
		}
		defer out.Close()
		log.SetOutput(out)
	}

	conf, err := os.Open(config)
	if err != nil {
		log.Fatal("Unable to open configuration:", err)
	}
	defer conf.Close()

	var c turtle.Config
	err = json.NewDecoder(conf).Decode(&c)
	if err != nil {
		log.Fatal("Unable to decode configuration:", err)
	}

	log.Print("Inspecting the turtle...")
	w := turtle.NewFilesetWalker(c)
	var matched bool

	before := time.Now()
	for {
		ok, err := w.HasMore()
		if err != nil {
			log.Fatal("Error traversing:", err)
		}
		if !ok {
			break
		}

		next := w.Walk()
		if m, s := matchesFilename(next, c); m {
			log.Printf("Filename Match: %s (%s)", next, s)
			matched = true
		}

		if c.InspectContent {
			m, s, err := matchesContent(next, c)
			if err != nil {
				log.Printf("Unable to inspect file %s: %s", next, err)
			}
			if m {
				log.Printf("Content Match: %s (%s)", next, s)
				matched = true
			}
		}
	}
	taken := time.Since(before)

	log.Printf("Took %s, processed %d files, %d bytes", taken, *fileCount, *byteCount)

	if matched {
		log.Println("Inspection finished, some parts stickin' out!")
	} else {
		log.Println("Inspection finished, looking good.")
	}
}

func matchesFilename(file string, c turtle.Config) (bool, string) {
	atomic.AddUint64(fileCount, 1)
	for _, s := range c.Keywords {
		if strings.Contains(file, s) {
			return true, s
		}
	}
	return false, ""
}

func matchesContent(file string, c turtle.Config) (bool, string, error) {
	f, err := turtle.Open(file)
	if err != nil {
		return false, "", err
	}
	defer f.Close()

	matchers := make([]turtle.ReadMatcher, len(c.Keywords))
	var last io.Reader = f

	for i, s := range c.Keywords {
		r := turtle.NewSearchReader(last, []byte(s))
		last = r
		matchers[i] = r
	}

	n, err := io.Copy(ioutil.Discard, last)
	if err != nil {
		return false, "", err
	}
	atomic.AddUint64(byteCount, uint64(n))

	for i, m := range matchers {
		if m.Matched() {
			return true, c.Keywords[i], nil
		}
	}
	return false, "", nil
}
