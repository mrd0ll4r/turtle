package turtle

import (
	"bytes"
	"io"
)

// wrappedReadCloser wraps an inner io.Reader and an outer io.ReadCloser to "skip" Close calls over the inner io.Reader.
// The inner Reader should be a decorator of the outer ReadCloser
type wrappedReadCloser struct {
	inner io.Reader
	outer io.ReadCloser
}

// Read reads from the inner reader
func (r *wrappedReadCloser) Read(p []byte) (n int, err error) {
	return r.inner.Read(p)
}

// Close closes the outer readCloser
func (r *wrappedReadCloser) Close() error {
	return r.outer.Close()
}

func newWrappedReader(inner io.Reader, outer io.ReadCloser) io.ReadCloser {
	return &wrappedReadCloser{
		inner: inner,
		outer: outer,
	}
}

// ReadMatcher is an io.Reader that can match things
type ReadMatcher interface {
	io.Reader
	// Matched indicates if the ReadMather matched something
	Matched() bool
}

type searchReader struct {
	r       io.Reader
	search  []byte
	buf     []byte
	len     int
	matched bool
}

// NewSearchReader returns a ReadMatcher that searches for some bytes in the io.Reader
func NewSearchReader(r io.Reader, search []byte) ReadMatcher {
	return &searchReader{
		r:      r,
		search: search,
		buf:    make([]byte, 0),
		len:    len(search),
	}
}

func (r *searchReader) Read(p []byte) (n int, err error) {
	for ; n < len(p); n++ {
		for len(r.buf) < r.len {
			//attempt to fill the buffer
			err = r.fill()
			if err != nil {
				if err == io.EOF {
					//we read nothing and the stream is over
					if len(r.buf) == 0 {
						//and we have nothing left to give them, so just
						return
					}
					break
				}
				return
			}
		}

		if len(r.buf) >= r.len {
			// we have enough to compare, so compare
			r.matched = r.matched || r.compare()
		}

		p[n] = r.buf[0]
		r.buf = r.buf[1:]
	}
	return
}

// compare compares the sliding window with whatever we search for
func (r *searchReader) compare() bool {
	return bytes.Equal(r.buf[:r.len], r.search)
}

// fill attempts to fill the sliding window
func (r *searchReader) fill() error {
	p := make([]byte, 2048)
	n, err := r.r.Read(p)
	if err != nil && err != io.EOF {
		return err
	}
	if n == 0 && err == io.EOF {
		return io.EOF
	}
	p = p[:n]
	r.buf = append(r.buf, p...)
	return nil
}

// Matched indicates if we encountered the searched-for content
func (r *searchReader) Matched() bool {
	return r.matched
}
