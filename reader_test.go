package turtle

import (
	"bytes"
	"io/ioutil"
	"testing"
)

func TestReader(t *testing.T) {
	testWith([]byte("abcdefgh"), []byte("abc"), true, t)
	testWith([]byte("abcdefgh"), []byte("ac"), false, t)
	testWith([]byte{1, 2, 3, 4, 5, 6}, []byte{3}, true, t)
	testWith([]byte{1, 2, 3, 4, 5}, []byte{1, 2, 3, 4, 5}, true, t)
	testWith([]byte{1, 2, 3, 4}, []byte{1, 2, 3, 4, 5}, false, t)
}

func testWith(content, search []byte, expect bool, t *testing.T) {
	buf := bytes.NewBuffer(nil)
	buf.Write(content)

	r := NewSearchReader(buf, search)

	got, err := ioutil.ReadAll(r)
	if err != nil {
		t.Fatal("Error reading:", err)
	}

	if r.Matched() != expect {
		t.Fatalf("Match expected: %t, got %t", expect, r.Matched())
	}

	if !bytes.Equal(got, content) {
		t.Fatalf("Reader changed the content, expected %s, got %s", content, got)
	}
}
